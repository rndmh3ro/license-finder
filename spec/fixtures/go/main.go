package main

import (
	"fmt"
	_ "github.com/dimfeld/httptreemux/v5"
	_ "github.com/go-logfmt/logfmt"
	_ "github.com/google/uuid"
	_ "github.com/stretchr/testify"
	_ "golang.org/x/oauth2"
)

func main() {
	fmt.Println("Hello, World!")
}
