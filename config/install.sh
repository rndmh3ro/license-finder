#!/bin/bash
set -euo pipefail

export DEBIAN_FRONTEND=noninteractive

echo -e "section_start:$(date +%s):install_packages\r\e[0K==> Installing packages…"
apt-get clean
apt-get update -q
apt-get install -y --no-install-recommends \
  apt-transport-https \
  autoconf \
  automake \
  bsdmainutils \
  bzip2 \
  ca-certificates \
  cmake \
  curl \
  git \
  gnupg2 \
  jq \
  make \
  pkg-config \
  re2c \
  rebar \
  sudo \
  unzip \
  zstd

dpkg --install /opt/toolcache/license*.deb
rm -fr /root
ln -s /opt/gitlab /root
echo -e "section_end:$(date +%s):install_packages\r\e[0K"

echo -e "section_start:$(date +%s):cleanup\r\e[0K==> Beginning cleanup…"
apt purge -y x11-common libx11-6 libgtk2.0-common libsensors-config ucf
apt autoremove -y

rm -fr /opt/gitlab/.config/configstore/update-notifier-npm.json \
  /opt/gitlab/.config/pip/selfcheck.json \
  /opt/gitlab/.gem \
  /opt/gitlab/.npm \
  /opt/gitlab/.wget-hsts \
  /etc/emacs/* \
  /etc/fonts/* \
  /etc/ldap/* \
  /etc/systemd/* \
  /etc/X11/* \
  /lib/systemd/* \
  /usr/lib/systemd/* \
  /usr/share/calendar/* \
  /usr/share/doc-base/* \
  /usr/share/emacs/* \
  /usr/share/fonts/* \
  /usr/share/icons/* \
  /usr/share/menu/* \
  /usr/share/pixmaps/* \
  /usr/share/themes/* \
  /usr/share/X11/* \
  /usr/share/zsh/* \
  /var/cache/* \
  /var/cache/apt/archives/ \
  /var/lib/apt/lists/* \
  /var/lib/systemd/* \
  /var/log/*
echo -e "section_end:$(date +%s):cleanup\r\e[0K"

echo -e "section_start:$(date +%s):compress_files\r\e[0K==> Starting compression…"
zstd_command="/usr/bin/zstd -19 -T0"

cd /opt/gitlab
tar --use-compress-program "$zstd_command" -cf /opt/gitlab/.config.tar.zst .config &
tar --use-compress-program "$zstd_command" -cf /opt/gitlab/.m2.tar.zst .m2 &
tar --use-compress-program "$zstd_command" -cf /opt/gitlab/embedded.tar.zst embedded &

cd /usr/lib
tar --use-compress-program "$zstd_command" -cf /usr/lib/git-core.tar.zst git-core &

wait
# shellcheck disable=SC2114
rm -fr \
  /opt/gitlab/.m2 \
  /opt/gitlab/.cache \
  /opt/gitlab/.config \
  /opt/gitlab/embedded \
  /usr/lib/git-core
echo -e "section_end:$(date +%s):compress_files\r\e[0K"
