# frozen_string_literal: true

python_version = ENV.fetch('PYTHON_VERSION', '3.8.5')

name "python-#{python_version}"
maintainer "GitLab B.V."
homepage "https://www.python.org/"

install_dir "/opt/asdf/installs/python/#{python_version}"
package_scripts_path Pathname.pwd.join("config/scripts/python")

build_version python_version
build_iteration 1

override 'asdf_python', version: python_version
dependency "asdf_python"

package :deb do
  compression_level 9
  compression_type :xz
end
