# frozen_string_literal: true
asdf_version = ENV.fetch('ASDF_VERSION', '0.7.8')

name "asdf"
maintainer "GitLab B.V."
homepage "https://github.com/asdf-vm/asdf"

install_dir "/opt/asdf"
package_scripts_path Pathname.pwd.join("config/scripts/asdf")

build_version asdf_version
build_iteration 1

override 'asdf', version: "v#{asdf_version}"
dependency "asdf"

package :deb do
  compression_level 9
  compression_type :xz
end
