# frozen_string_literal: true

name "asdf_dotnet_sdk"
default_version "3.1.302"
whitelist_file(%r{shared/.*/.*\.so\z})

dependency "curl"
dependency "libffi"
dependency "zlib"

source url: "https://dotnetcli.azureedge.net/dotnet/Sdk/#{version}/dotnet-sdk-#{version}-linux-x64.tar.gz"
relative_path ""

version "3.1.302" do
  source sha256: "777f376b65974e459e8804671461747c53b4eb6917f3e28db2cf6f9ed4be23c8"
end
version "3.1.301" do
  source sha256: "a0c416bb57f7e16a4767e7cd4dbeab80386fffc354ee7cdc68d367694bc40269"
end

build do
  mkdir install_dir
  sync "#{project_dir}/", install_dir
end
