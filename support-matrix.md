# License Compliance analyzer support matrix

| Date | GitLab version | Analyzer version|
| --- | --- | --- |
| 2022-05-22 | 15.0 | 4.x |
| [2020-03-18](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/tags/v3.0.0) | 12.9 - 14.10 | [3.x](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/blob/main/CHANGELOG.md#v300) |
| [2019-11-15](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/tags/v2.0.0) | 12.5 - 12.8 | [2.x](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/blob/main/CHANGELOG.md#v200) |
| [2018-12-20](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/tags/v1.0.0) | 11.6 - 12.4 | [1.x](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/blob/main/CHANGELOG.md#v100) |
