# frozen_string_literal: true

module License
  module Management
    module Loggable
      def logger
        License::Management.logger
      end
    end
  end
end
