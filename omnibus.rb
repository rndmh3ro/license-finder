# frozen_string_literal: true

base_dir './tmp'
cache_dir     './tmp/omnibus/cache'
git_cache_dir './tmp/omnibus/cache/git_cache'
source_dir    './tmp/omnibus/src'
build_dir     './tmp/omnibus/build'
package_dir   './tmp/omnibus/pkg'
package_tmp   './tmp/omnibus/pkg-tmp'
append_timestamp false
software_gems ['omnibus-software']
